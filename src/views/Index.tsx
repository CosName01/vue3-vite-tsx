import { reactive, ref, watch } from 'vue';
import { GetBusinessCustomerRelation } from '/@/api/AuthLoginApi';
export default {
  setup() {
    const state = reactive<{
      val: number;
    }>({
      val: 1111,
    });
    const visible = ref(true);

    watch(
      () => state.val,
      (val, oldVal) => {
        console.log(val, oldVal);
      }
    );
    const mockPost = () => {
      var params = {
        bizSequenceId: '20210205095025550400',
        customerCode: '100',
      };
      GetBusinessCustomerRelation(params)
        .then((res) => {
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
      state.val = 222;
    };
    return () => (
      <section>
        <input v-show={visible} type="text" v-model={state.val} /> {state.val}
        <input v-show={!visible} v-model={state.val} />
        <button class="dsa" onClick={mockPost}>
          dsadsa
        </button>
      </section>
    );
  },
};
