import axios from './axios'
export function 
  get(url: string, query: any){
    return axios({
      url: url,
      method: 'GET',
      headers: {'Content-Type': 'application/json;charset=UTF-8'},
      data: query,
    })
  }

export function 
  post(url: string, query: any){
    return axios({
      url: url,
      method: 'POST',
      headers: {'Content-Type': 'application/json;charset=UTF-8'},
      data: query,
    })
  }
