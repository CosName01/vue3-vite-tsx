import { createApp } from 'vue';
import router from '/@/router';
import store from '/@/store';
import App from '/@/App.tsx';
import '/@/styles/index.scss';
import bottom from '/@/components/bottom/bottom';
console.log(store);
const app = createApp(App);
//注册全局组件
const component = [bottom];
component.forEach((item) => {
  app.component(item.name, item);
});
app.use(router);
app.use(store);
app.mount('#app');
