import { defineComponent, ref, reactive } from 'vue';
import './bottom.scss';
export default defineComponent({
  name: 'bottom',
  props: {
    index: {
      type: Number,
      default: 2,
    },
  },

  setup(props) {
    const iconList = [
      {
        name: '首页',
        url: '123123',
        bgColor: '#green',
      },
      {
        name: '财富',
        url: '123123',
        bgColor: '#green',
      },
      {
        name: '信用卡',
        url: '123123',
        bgColor: '#green',
      },
      {
        name: '生活',
        url: '123123',
        bgColor: '#green',
      },
      {
        name: '我的',
        url: '123123',
        bgColor: '#green',
      },
    ];
    return () => (
      <div class="bottomBg">
        {iconList.map((item) => {
          <div class="iconBg">
            <div class="icon">
              <img src={item.url} alt="" />
            </div>
          </div>;
        })}
      </div>
    );
  },
});
